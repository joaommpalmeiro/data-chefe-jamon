# data-chefe-jamon

## Notes

- The score is from 1 to 10.
- https://www.fatsecret.pt/calorias-nutri%C3%A7%C3%A3o/
- https://www.notion.so/help/import-data-into-notion
- https://stedolan.github.io/jq/ (`sudo apt-get install jq`)
- https://e.printstacktrace.blog/how-to-convert-json-to-csv-from-the-command-line/
- https://csvjson.com/json2csv
- https://tienda.mercadona.es/

## Snippets

```json
{
  "product": "",
  "retailer": "",
  "score": 0,
  "tweet": "",
  "info": ""
}
```
